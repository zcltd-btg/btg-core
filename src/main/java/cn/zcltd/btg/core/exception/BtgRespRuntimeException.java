package cn.zcltd.btg.core.exception;

/**
 * 安心付自定义异常
 */
public class BtgRespRuntimeException extends BtgRuntimeException {
    public BtgRespRuntimeException() {
        super();
    }

    public BtgRespRuntimeException(String message) {
        super(message);
    }

    public BtgRespRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BtgRespRuntimeException(Throwable cause) {
        super(cause);
    }

    protected BtgRespRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BtgRespRuntimeException(String code, String desc) {
        super(code, desc);
    }

    public BtgRespRuntimeException(String code, String desc, Throwable cause) {
        super(code, desc, cause);
    }
}