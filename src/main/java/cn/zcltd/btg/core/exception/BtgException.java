package cn.zcltd.btg.core.exception;

/**
 * 自定义异常
 */
public class BtgException extends Exception {
    public static final String DEFAULT_CODE = "9999";
    public static final String DEFAULT_DESC = "请求出错";

    private String code;
    private String desc;

    public BtgException() {
        super();
        this.code = DEFAULT_CODE;
        this.desc = DEFAULT_DESC;
    }

    public BtgException(String message) {
        super(message);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgException(String message, Throwable cause) {
        super(message, cause);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgException(Throwable cause) {
        super(cause);
        this.code = DEFAULT_CODE;
        this.desc = DEFAULT_DESC;
    }

    protected BtgException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgException(String code, String desc) {
        super(code + "#" + desc);
        this.code = code;
        this.desc = desc;
    }

    public BtgException(String code, String desc, Throwable cause) {
        super(code + "#" + desc, cause);
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}