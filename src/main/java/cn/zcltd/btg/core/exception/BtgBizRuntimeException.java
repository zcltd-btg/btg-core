package cn.zcltd.btg.core.exception;

/**
 * 安心付自定义异常
 */
public class BtgBizRuntimeException extends BtgRuntimeException {

    public BtgBizRuntimeException() {
        super();
    }

    public BtgBizRuntimeException(String message) {
        super(message);
    }

    public BtgBizRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BtgBizRuntimeException(Throwable cause) {
        super(cause);
    }

    protected BtgBizRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BtgBizRuntimeException(String code, String desc) {
        super(code, desc);
    }

    public BtgBizRuntimeException(String code, String desc, Throwable cause) {
        super(code, desc, cause);
    }
}