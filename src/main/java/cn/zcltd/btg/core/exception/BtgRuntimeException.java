package cn.zcltd.btg.core.exception;

/**
 * 自定义运行时异常
 */
public class BtgRuntimeException extends RuntimeException {
    public static final String DEFAULT_CODE = "9999";
    public static final String DEFAULT_DESC = "请求出错";

    private String code;
    private String desc;

    public BtgRuntimeException() {
        super();
        this.code = DEFAULT_CODE;
        this.desc = DEFAULT_DESC;
    }

    public BtgRuntimeException(String message) {
        super(message);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgRuntimeException(Throwable cause) {
        super(cause);
        this.code = DEFAULT_CODE;
        this.desc = DEFAULT_DESC;
    }

    protected BtgRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = DEFAULT_CODE;
        this.desc = message;
    }

    public BtgRuntimeException(String code, String desc) {
        super(code + "#" + desc);
        this.code = code;
        this.desc = desc;
    }

    public BtgRuntimeException(String code, String desc, Throwable cause) {
        super(code + "#" + desc, cause);
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}